package cz.cvut.fit.miadp.mvcgame;

import java.util.List;

import cz.cvut.fit.miadp.mvcgame.bridge.IGameGraphics;
import cz.cvut.fit.miadp.mvcgame.command.Invoker;
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.controller.InputController;
import cz.cvut.fit.miadp.mvcgame.model.GameModel;
import cz.cvut.fit.miadp.mvcgame.model.Model;
import cz.cvut.fit.miadp.mvcgame.view.GameView;

public class MvcGame {
    private Model gameModel;
    private InputController inputController;
    private Invoker invoker;
    private GameView view;

    public void init() {
        gameModel = new GameModel();
        gameModel.generateEnemies();
        view = new GameView(gameModel);
        inputController = view.createController();

        invoker = new Invoker();
        gameModel.setInvoker(invoker);
        inputController.setInvoker(invoker);
    }

    public void processPressedKeys(List<String> pressedKeysCodes) {
        for (String code : pressedKeysCodes) {
            inputController.handlePressedKey(code);
        }
    }

    public void update() {
        // nothing yet
    }

    public void setGraphics(IGameGraphics gg) {
        this.view.setGraphics(gg);
    }

    public void render() {
        this.view.render();
    }

    public String getWindowTitle() {
        return "The MI-ADP.16 MvcGame";
    }

    public int getWindowWidth() {
        return MvcGameConfig.MAX_X;
    }

    public int getWindowHeight() {
        return MvcGameConfig.MAX_Y;
    }
}