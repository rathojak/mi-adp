package cz.cvut.fit.miadp.mvcgame.abstractFactory;

import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractCannon;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractCollision;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractEnemy;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractGameInfo;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractMissile;
import cz.cvut.fit.miadp.mvcgame.strategy.MissileMovingStrategy;

/**
 * AbstractFactory
 */
public interface AbstractFactory {
    public abstract AbstractCannon createCannon();
    public abstract AbstractEnemy createEnemy();
    public abstract AbstractCollision createCollistion();
    public abstract AbstractMissile createMissile();
    public abstract AbstractGameInfo createGameInfo();

    void setpMissileProductionStrategy(MissileMovingStrategy mms);
	public abstract MissileMovingStrategy getStrategy();
	public abstract AbstractEnemy createAdvanceEnemy();
}