package cz.cvut.fit.miadp.mvcgame.abstractFactory;

import java.util.Random;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Model;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractCannon;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractCollision;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractEnemy;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractGameInfo;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractMissile;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.defaultGO.AdvanceEnemy;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.defaultGO.Cannon;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.defaultGO.Collision;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.defaultGO.Enemy;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.defaultGO.GameInfo;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.defaultGO.Missile;
import cz.cvut.fit.miadp.mvcgame.strategy.MissileMovingStrategy;
import cz.cvut.fit.miadp.mvcgame.strategy.SimpleMissileMoveStrategy;

/**
 * DefaultFamilyFactory
 */
public class DefaultFamilyFactory implements AbstractFactory {
    protected Model model;
    protected MissileMovingStrategy mms = new SimpleMissileMoveStrategy();

    public DefaultFamilyFactory(Model model) {
        this.model = model;
    }

    @Override
    public AbstractCannon createCannon() {
        return new Cannon(this);
    }

    @Override
    public AbstractEnemy createEnemy() {
        AbstractEnemy enm = new Enemy();
        Random rnd = new Random();
        int xBoundIndex = MvcGameConfig.MAX_X / 50;
        int yBoundIndex = MvcGameConfig.MAX_Y / 25;
        int posX = rnd.nextInt(xBoundIndex - 1) * 50 + 50;
        int posY = rnd.nextInt(yBoundIndex - 1) * 25 + 25;
        enm.setPosition(new Position(posX, posY));
        return enm;
    }

    @Override
    public AbstractCollision createCollistion() {
        return new Collision();
    }

    @Override
    public AbstractMissile createMissile() {
        AbstractMissile msl = new Missile(this.mms);

        model.addMissile(msl);

        return msl;
    }

    @Override
    public AbstractGameInfo createGameInfo() {
        return new GameInfo();
    }

    public void setpMissileProductionStrategy(MissileMovingStrategy mms) {
        this.mms = mms;
    }

    @Override
    public MissileMovingStrategy getStrategy() {
        return this.mms;
    }

    @Override
    public AbstractEnemy createAdvanceEnemy() {
        AbstractEnemy enm = new AdvanceEnemy();
        Random rnd = new Random();
        int xBoundIndex = MvcGameConfig.MAX_X / 50;
        int yBoundIndex = MvcGameConfig.MAX_Y / 25;
        int posX = rnd.nextInt(xBoundIndex - 1) * 50 + 50;
        int posY = rnd.nextInt(yBoundIndex - 1) * 25 + 25;
        enm.setPosition(new Position(posX, posY));
        return enm;
    }
}