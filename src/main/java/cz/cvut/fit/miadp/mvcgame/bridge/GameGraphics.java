package cz.cvut.fit.miadp.mvcgame.bridge;

import cz.cvut.fit.miadp.mvcgame.model.Position;

/**
 * GameGraphics
 */
public class GameGraphics implements IGameGraphics {
    private GameGraphicsImplementor implementor;

    public GameGraphics(GameGraphicsImplementor implementor) {
        this.implementor = implementor;
    }

    @Override
    public void drawImage(String path, Position pos) {
        this.implementor.drawImage(path, pos);
    }

    @Override
    public void drawText(String text, Position pos) {
        this.implementor.drawText(text, pos);
    }

    @Override
    public void drawRectangle(Position leftTop, Position rightBottom) {
        this.implementor.drawLine(leftTop, new Position(rightBottom.getX(), leftTop.getY()));
        this.implementor.drawLine(leftTop, new Position(leftTop.getX(), rightBottom.getY()));
        this.implementor.drawLine(new Position(leftTop.getX(), rightBottom.getY()), rightBottom);
        this.implementor.drawLine(new Position(rightBottom.getX(), leftTop.getY()), rightBottom);
    }

    @Override
    public void clear(Position beginPos, Position endPos) {
        this.implementor.clear(beginPos, endPos);
    }
}