package cz.cvut.fit.miadp.mvcgame.bridge;

import cz.cvut.fit.miadp.mvcgame.model.Position;

/**
 * GameGraphicsImplementor
 */
public interface GameGraphicsImplementor {
    void drawImage(String path, Position pos);
    void drawText(String text, Position pos);
    void drawLine(Position beginPos, Position endPos);

    void clear(Position beginPos, Position endPos);
}