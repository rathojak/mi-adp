package cz.cvut.fit.miadp.mvcgame.command;

import cz.cvut.fit.miadp.mvcgame.model.Model;

/**
 * AngleDownCommand
 */
public class AngleDownCommand extends CommandBase {

    public AngleDownCommand(Model receiver) {
        super(receiver);
    }

    @Override
    protected void execute() {
        this.receiver.aimDown();
    }
}