package cz.cvut.fit.miadp.mvcgame.command;

import cz.cvut.fit.miadp.mvcgame.model.Model;

/**
 * AngleUpCommand
 */
public class AngleUpCommand extends CommandBase {

    public AngleUpCommand(Model receiver) {
        super(receiver);
    }

    @Override
    protected void execute() {
        this.receiver.aimUp();
    }
}