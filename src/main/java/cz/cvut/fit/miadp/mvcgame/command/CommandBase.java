package cz.cvut.fit.miadp.mvcgame.command;

import cz.cvut.fit.miadp.mvcgame.model.Model;

public abstract class CommandBase {
    protected Model receiver;
    protected Object memento;

    public CommandBase(Model receiver) {
        this.receiver = receiver;
    }

    protected abstract void execute();

    public void doExecute() {
        this.memento = this.receiver.createMement();
        this.execute();
    }

    public void unExecute() {
        this.receiver.setMemento(this.memento);
    }
}