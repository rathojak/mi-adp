package cz.cvut.fit.miadp.mvcgame.command;

import java.util.Stack;
import java.util.concurrent.LinkedBlockingDeque;

// objekt, který bude mít na starosti historii příkazů a jejich vykonání
public class Invoker {
    private Stack<CommandBase> executedCommand;
    private LinkedBlockingDeque<CommandBase> notExecutedCommand;

    public Invoker() {
        this.executedCommand = new Stack<>();
        this.notExecutedCommand = new LinkedBlockingDeque<CommandBase>();
    }

    public void addCommand(CommandBase command) {
        notExecutedCommand.add(command);
    }

    public void undoCommand() {
        if (executedCommand.size() > 0) {
            CommandBase command = executedCommand.pop();
            command.unExecute();
        }
    }

    public void invokeNextCommand() {
        if (notExecutedCommand.size() > 0) {
            CommandBase command = notExecutedCommand.poll();
            command.doExecute();
            executedCommand.add(command);
        }
    }
}