package cz.cvut.fit.miadp.mvcgame.command;

import java.util.ArrayList;
import java.util.List;

import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractCannon;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractCollision;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractEnemy;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractGameInfo;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractMissile;

/**
 * Memento
 */
public class Memento {
    private int score;
    private AbstractCannon cannon;
    private AbstractGameInfo info;

    private List<AbstractEnemy> enemies;
    private List<AbstractMissile> missiles;
    private List<AbstractCollision> collisions;

    public int getScore() {
        return score;
    }

    public List<AbstractMissile> getMissiles() {
        return missiles;
    }

    public void setMissiles(List<AbstractMissile> missiles) {
        this.missiles = new ArrayList<>();

        for (AbstractMissile abstractMissile : missiles) {
            this.missiles.add(abstractMissile.clone());
        }
    }

    public List<AbstractEnemy> getEnemies() {
        return enemies;
    }

    public void setEnemies(List<AbstractEnemy> enemies) {
        this.enemies = new ArrayList<>();
        for (AbstractEnemy abstractEnemy : enemies) {
            this.enemies.add(abstractEnemy.clone());
        }
    }

    public AbstractGameInfo getInfo() {
        return info;
    }

    public void setInfo(AbstractGameInfo info) {
        this.info = info.clone();
    }

    public AbstractCannon getCannon() {
        return cannon;
    }

    public void setCannon(AbstractCannon cannon) {
        this.cannon = cannon.clone();
    }

    public void setScore(int score) {
        this.score = score;
    }

	public void setCollisions(List<AbstractCollision> collisions) {
        this.collisions = new ArrayList<>();
        for (AbstractCollision abstractCollision : collisions) {
            this.collisions.add(abstractCollision.clone());
        };
    }
    public List<AbstractCollision> getCollisions() {
        return this.collisions;
    }
}