package cz.cvut.fit.miadp.mvcgame.command;

import cz.cvut.fit.miadp.mvcgame.model.Model;

public class MoveDownCommand extends CommandBase {
    public MoveDownCommand(Model receiver) {
        super(receiver);
    }

    @Override
    public void execute() {
        this.receiver.moveDown();
    }
}