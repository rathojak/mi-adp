package cz.cvut.fit.miadp.mvcgame.command;

import cz.cvut.fit.miadp.mvcgame.model.Model;

public class MoveUpCommand extends CommandBase {
    public MoveUpCommand(Model receiver) {
        super(receiver);
    }

    @Override
    public void execute() {
        this.receiver.moveUp();
    }
}