package cz.cvut.fit.miadp.mvcgame.command;

import cz.cvut.fit.miadp.mvcgame.model.Model;

public class PowerDownCommand extends CommandBase {

    public PowerDownCommand(Model receiver) {
        super(receiver);
        // TODO Auto-generated constructor stub
    }

    @Override
    protected void execute() {
        this.receiver.PowerDown();
    }

}