package cz.cvut.fit.miadp.mvcgame.command;

import cz.cvut.fit.miadp.mvcgame.model.Model;

/**
 * PowerUpCommand
 */
public class PowerUpCommand extends CommandBase {

    public PowerUpCommand(Model receiver) {
        super(receiver);
        // TODO Auto-generated constructor stub
    }

    @Override
    protected void execute() {
        this.receiver.PowerUp();
    }
}