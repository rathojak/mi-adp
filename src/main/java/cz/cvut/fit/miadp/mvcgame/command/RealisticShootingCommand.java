package cz.cvut.fit.miadp.mvcgame.command;

import cz.cvut.fit.miadp.mvcgame.model.Model;

/**
 * ShootCommand
 */
public class RealisticShootingCommand extends CommandBase {

    public RealisticShootingCommand(Model receiver) {
        super(receiver);
    }

    @Override
    protected void execute() {
        this.receiver.setRealisticShooting();
    }
}