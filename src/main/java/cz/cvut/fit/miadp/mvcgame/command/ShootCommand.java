package cz.cvut.fit.miadp.mvcgame.command;

import cz.cvut.fit.miadp.mvcgame.model.Model;

/**
 * ShootCommand
 */
public class ShootCommand extends CommandBase {

    public ShootCommand(Model receiver) {
        super(receiver);
    }

    @Override
    protected void execute() {
        this.receiver.shoot();
    }
}