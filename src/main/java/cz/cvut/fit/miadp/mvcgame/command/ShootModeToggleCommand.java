package cz.cvut.fit.miadp.mvcgame.command;

import cz.cvut.fit.miadp.mvcgame.model.Model;

/**
 * ShootModeToogleCommand
 */
public class ShootModeToggleCommand extends CommandBase {

    public ShootModeToggleCommand(Model receiver) {
        super(receiver);
    }

    @Override
    protected void execute() {
        this.receiver.toggleShootMode();
    }
}