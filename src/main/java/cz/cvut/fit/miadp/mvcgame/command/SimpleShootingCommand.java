package cz.cvut.fit.miadp.mvcgame.command;

import cz.cvut.fit.miadp.mvcgame.model.Model;

/**
 * ShootCommand
 */
public class SimpleShootingCommand extends CommandBase {

    public SimpleShootingCommand(Model receiver) {
        super(receiver);
    }

    @Override
    protected void execute() {
        this.receiver.setSimpleShooting();
    }
}