package cz.cvut.fit.miadp.mvcgame.config;

public class MvcGameConfig {
    public static final int MAX_X = 1280;
    public static final int MAX_Y = 720;

    public static final int ANGLE_STEP = 3;
    public static final int CANNON_MOVE_STEP = 10;

    public static final int MAX_ENEMIES = 4;

    public static final int INFO_POS_X = 90;
    public static final int INFO_POS_Y = 40;

    public static final int CANNON_POS_X = 0;
    public static final int CANNON_POS_y = MAX_Y / 2;

    public static final int TIME_TICK_PERIOD = 5;
    public static final int SIZE_OF_MISSILE_PICTURE = 30;

	public static final int ENEMY_STEP_SIZE = 15;
}