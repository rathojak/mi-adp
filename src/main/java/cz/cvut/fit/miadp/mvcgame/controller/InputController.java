package cz.cvut.fit.miadp.mvcgame.controller;

import cz.cvut.fit.miadp.mvcgame.command.AngleDownCommand;
import cz.cvut.fit.miadp.mvcgame.command.AngleUpCommand;
import cz.cvut.fit.miadp.mvcgame.command.CommandBase;
import cz.cvut.fit.miadp.mvcgame.command.Invoker;
import cz.cvut.fit.miadp.mvcgame.command.MoveDownCommand;
import cz.cvut.fit.miadp.mvcgame.command.MoveUpCommand;
import cz.cvut.fit.miadp.mvcgame.command.PowerDownCommand;
import cz.cvut.fit.miadp.mvcgame.command.PowerUpCommand;
import cz.cvut.fit.miadp.mvcgame.command.RealisticShootingCommand;
import cz.cvut.fit.miadp.mvcgame.command.ShootCommand;
import cz.cvut.fit.miadp.mvcgame.command.ShootModeToggleCommand;
import cz.cvut.fit.miadp.mvcgame.command.SimpleShootingCommand;
import cz.cvut.fit.miadp.mvcgame.model.Model;

public class InputController {
    private Model gameModel;
    private Invoker invoker;
    private long lastShootTime = 0;

    public InputController(Model gameModel) {
        this.gameModel = gameModel;
    }

    public void setInvoker(Invoker invoker) {
        this.invoker = invoker;
    }

    // bez tohohle limitu, se může držet mezerník a lítá miliarda střel
    public boolean canShoot() {
        long now = System.currentTimeMillis();
        if (now - lastShootTime > 1000) {
            this.lastShootTime = now;
            return true;
        }
        return false;
    }

    // metoda na základě vstupu vytvoří command, který bude handlovat, co se bude
    // dít
    public void handlePressedKey(String pressedKeysCodes) {
        CommandBase command;
        switch (pressedKeysCodes) {
        case "W":
            command = new MoveUpCommand(gameModel);
            invoker.addCommand(command);
            break;
        case "S":
            command = new MoveDownCommand(gameModel);
            invoker.addCommand(command);
            break;
        case "UP":
            command = new AngleUpCommand(gameModel);
            invoker.addCommand(command);
            break;
        case "DOWN":
            command = new AngleDownCommand(gameModel);
            invoker.addCommand(command);
            break;
        case "RIGHT":
            command = new SimpleShootingCommand(gameModel);
            invoker.addCommand(command);
            break;
        case "LEFT":
            command = new RealisticShootingCommand(gameModel);
            invoker.addCommand(command);
            break;
        case "SPACE":
            if (this.canShoot()) {
                command = new ShootCommand(gameModel);
                invoker.addCommand(command);
            }
            break;
        case "Q":
            invoker.undoCommand();
            break;
        case "E":
            command = new ShootModeToggleCommand(gameModel);
            invoker.addCommand(command);
            break;
        case "A":
            command = new PowerUpCommand(gameModel);
            invoker.addCommand(command);
            break;
        case "D":
            command = new PowerDownCommand(gameModel);
            invoker.addCommand(command);
            break;
        default:
            // nothing
        }
    }
}