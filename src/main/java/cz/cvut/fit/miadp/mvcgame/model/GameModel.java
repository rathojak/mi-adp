package cz.cvut.fit.miadp.mvcgame.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Random;

import cz.cvut.fit.miadp.mvcgame.abstractFactory.AbstractFactory;
import cz.cvut.fit.miadp.mvcgame.abstractFactory.DefaultFamilyFactory;
import cz.cvut.fit.miadp.mvcgame.command.Invoker;
import cz.cvut.fit.miadp.mvcgame.command.Memento;
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractCannon;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractCollision;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractEnemy;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractGameInfo;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractMissile;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.GameObjectBase;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.defaultGO.GameInfo;
import cz.cvut.fit.miadp.mvcgame.observer.Observer;
import cz.cvut.fit.miadp.mvcgame.strategy.RealisticMissileMoveStrategy;
import cz.cvut.fit.miadp.mvcgame.strategy.SimpleMissileMoveStrategy;

// Hlavní objekt, který přenáší data mezi logikou a view
public class GameModel implements Model {
    private int score;
    private AbstractCannon cannon;
    private AbstractGameInfo info;
    private AbstractFactory goFactory;
    private Invoker invoker;
    private Timer timer;

    private List<AbstractEnemy> enemies;
    private List<AbstractMissile> missiles;
    private List<AbstractCollision> collisions;
    private List<Observer> observers;

    private int counter = 0;

    public GameModel() {
        this.goFactory = new DefaultFamilyFactory(this);
        this.cannon = goFactory.createCannon();
        this.info = new GameInfo();
        initTimer();

        observers = new ArrayList<Observer>();
        enemies = new ArrayList<AbstractEnemy>();
        missiles = new ArrayList<AbstractMissile>();
        collisions = new ArrayList<AbstractCollision>();
    }

    private void initTimer() {
        this.timer = new Timer();
        this.timer.schedule(new TimerTask() {
            @Override
            public void run() {
                timeTick();
            }
        }, 0, MvcGameConfig.TIME_TICK_PERIOD);
    }

    public void timeTick() {
        if (invoker != null) {
            counter++;
            this.deleteColisions();
            this.moveEnemy();
            this.invoker.invokeNextCommand();
            this.moveMissilies();
            this.findDeadEnemies();
            this.getInfoText();
            this.notifyObservers();
        }
    }

    private void moveEnemy() {
        if (this.counter % 40 == 0) {
            this.counter = 0;
            for (AbstractEnemy en : this.enemies) {
                en.move();
            }
        }
    }

    private void deleteColisions() {
        List<AbstractCollision> deactivatedCollisions = new ArrayList<>();
        for (AbstractCollision abstractCollision : collisions) {
            if (abstractCollision.getAge() > 1)
                deactivatedCollisions.add(abstractCollision);
        }
        for (AbstractCollision collision : deactivatedCollisions) {
            this.collisions.remove(collision);
        }
    }

    private void findDeadEnemies() {
        List<AbstractEnemy> deadEnemies = new ArrayList<>();
        List<AbstractMissile> deactivatedMissile = new ArrayList<>();
        for (AbstractEnemy enemy : this.enemies) {
            for (AbstractMissile missile : this.missiles) {
                if (missile.getPosition().isInRadius(enemy.getPosition())) {
                    deadEnemies.add(enemy);
                    deactivatedMissile.add(missile);

                    AbstractCollision collision = this.goFactory.createCollistion();
                    collision.setPosition(enemy.getPosition().clone());
                    this.collisions.add(collision);
                    this.score++;
                    break;
                }
            }
        }
        for (AbstractMissile abstractMissile : deactivatedMissile) {
            this.missiles.remove(abstractMissile);
        }
        for (AbstractEnemy enemy : deadEnemies) {
            this.enemies.remove(enemy);
        }
        if (enemies.size() == 0) {
            this.generateEnemies();
        }
    }

    private void moveMissilies() {
        Position leftCorner = new Position(0, 0);
        Position rightDown = new Position(MvcGameConfig.MAX_X, MvcGameConfig.MAX_Y);
        List<AbstractMissile> outOfBorder = new ArrayList<>();
        for (AbstractMissile mis : this.missiles) {
            mis.move();

            if (mis.getPosition().isOut(leftCorner, rightDown)) {
                outOfBorder.add(mis);
            }
        }
        for (AbstractMissile abstractMissile : outOfBorder) {
            this.missiles.remove(abstractMissile);
        }
    }

    public void generateEnemies() {
        enemies.clear();

        int numberOfAdvanced = (int)(score / MvcGameConfig.MAX_ENEMIES);
        ;
        for (int i = 0; i < numberOfAdvanced; i++) {
            enemies.add(goFactory.createAdvanceEnemy());
        }

        for (int i = this.enemies.size(); i < MvcGameConfig.MAX_ENEMIES; i++) {
            enemies.add(goFactory.createEnemy());
        }
    }

    public void moveUp() {
        this.cannon.moveUp();
    }

    public void moveDown() {
        this.cannon.moveDown();
    }

    public String getInfoText() {
        String text = "Score: " + this.score + "\nAngle: " + this.cannon.getAngle() + " \nPower: "
                + this.cannon.getPower() + "\nShooting mode: " + this.cannon.getShootingMode().getName()
                + "\nMissile strategy: " + this.goFactory.getStrategy().getName();
        this.info.setText(text);
        return text;
    }

    @Override
    public void setMemento(Object memento) {
        Memento mem = (Memento) memento;
        this.cannon = mem.getCannon();
        this.info = mem.getInfo();
        this.score = mem.getScore();
        this.enemies = mem.getEnemies();
        this.missiles = mem.getMissiles();
        this.collisions = mem.getCollisions();
        this.notifyObservers();
    }

    @Override
    public Object createMement() {
        Memento mem = new Memento();
        mem.setEnemies(this.enemies);
        mem.setCannon(this.cannon);
        mem.setInfo(this.info);
        mem.setScore(this.score);
        mem.setMissiles(this.missiles);
        mem.setCollisions(this.collisions);
        return mem;
    }

    @Override
    public List<GameObjectBase> getGameObjects() {
        List<GameObjectBase> objs = new ArrayList<GameObjectBase>();
        objs.add(this.cannon);
        objs.add(this.info);
        objs.addAll(this.enemies);
        objs.addAll(this.missiles);
        objs.addAll(this.collisions);
        return objs;
    }

    @Override
    public void notifyObservers() {
        for (Observer obs : this.observers) {
            obs.update();
        }
    }

    @Override
    public void attach(Observer observer) {
        this.observers.add(observer);
    }

    @Override
    public void detach(Observer observer) {
        this.observers.remove(observer);
    }

    public void setInvoker(Invoker invoker) {
        this.invoker = invoker;
    }

    @Override
    public void shoot() {
        this.missiles.addAll(this.cannon.shoot());
        this.notifyObservers();
    }

    @Override
    public void aimUp() {
        this.cannon.aimUp();
    }

    @Override
    public void aimDown() {
        this.cannon.aimDown();
    }

    @Override
    public void addMissile(AbstractMissile msl) {
        this.missiles.add(msl);
    }

    @Override
    public void toggleShootMode() {
        this.cannon.toggleMode();
    }

    @Override
    public void setRealisticShooting() {
        this.goFactory.setpMissileProductionStrategy(new RealisticMissileMoveStrategy());

    }

    @Override
    public void setSimpleShooting() {
        this.goFactory.setpMissileProductionStrategy(new SimpleMissileMoveStrategy());
    }

    @Override
    public void PowerUp() {
        this.cannon.IncreasePower();
    }

    @Override
    public void PowerDown() {
        this.cannon.DecreasePower();
    }
}