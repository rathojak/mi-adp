package cz.cvut.fit.miadp.mvcgame.model;

import java.util.List;

import cz.cvut.fit.miadp.mvcgame.command.Invoker;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractMissile;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.GameObjectBase;
import cz.cvut.fit.miadp.mvcgame.observer.Observable;

public interface Model extends Observable{
    void moveUp();
    void moveDown();

    List<GameObjectBase> getGameObjects();
    void generateEnemies();

    void setInvoker(Invoker invoker);

    void setMemento(Object memento);
    Object createMement();
	void shoot();
	void aimUp();
	void aimDown();
    void addMissile(AbstractMissile msl);

    void toggleShootMode();
	void setRealisticShooting();
	void setSimpleShooting();
	void PowerUp();
	void PowerDown();
}