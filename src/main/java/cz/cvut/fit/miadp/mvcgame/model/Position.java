package cz.cvut.fit.miadp.mvcgame.model;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.prototype.Prototype;

public class Position implements Prototype<Position> {
	private int dimX = 0;
	private int dimY = 0;

	public Position() {
	}

	public Position(int posX, int posY) {
		this.dimX = posX;
		this.dimY = posY;
	}

	public int getX() {
		return dimX;
	}

	public int getY() {
		return dimY;
	}

	public void setY(int y) {
		this.dimY = y;
	}

	public void setX(int x) {
		this.dimX = x;
	}

	public Boolean isOut(Position leftTop, Position rightDown) {
		if (this.getX() < leftTop.getX() || this.getX() > rightDown.getX())
			return true;
		if (this.getY() < leftTop.getY() || this.getY() > rightDown.getY())
			return true;
		return false;
	}

	public Boolean isInRadius(Position pos) {
		if (Math.abs(this.dimX - pos.getX()) <= MvcGameConfig.SIZE_OF_MISSILE_PICTURE && Math.abs(this.dimY - pos.getY()) <= MvcGameConfig.SIZE_OF_MISSILE_PICTURE)
			return true;
		return false;
	}

	public Position clone() {
		Position copy = new Position();
		copy.setX(this.dimX);
		copy.setY(this.dimY);
		return copy;
	}
}