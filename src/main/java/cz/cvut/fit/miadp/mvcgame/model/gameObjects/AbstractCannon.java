package cz.cvut.fit.miadp.mvcgame.model.gameObjects;

import java.util.List;

import cz.cvut.fit.miadp.mvcgame.abstractFactory.AbstractFactory;
import cz.cvut.fit.miadp.mvcgame.prototype.Prototype;
import cz.cvut.fit.miadp.mvcgame.state.cannonShootState.ShootingMode;

public abstract class AbstractCannon extends GameObjectBase implements Prototype<AbstractCannon> {
    public AbstractCannon(AbstractFactory fac) {
        this.fac = fac;
    }
    protected AbstractFactory fac;
    protected int power;
    protected int angle;

    protected ShootingMode shootingMode;

    public abstract void moveUp();
    public abstract void moveDown();

    public abstract void aimUp();
    public abstract void aimDown();

    public abstract List<AbstractMissile> shoot();

    public abstract AbstractMissile getMissile();

    public void toggleMode() {
        this.shootingMode.nextMode(this);
    }
    public void setShootingMode(ShootingMode shootingMode) {
        this.shootingMode = shootingMode;
    }

    public ShootingMode getShootingMode() {
        return this.shootingMode;
    }

    public int getAngle() {
        return this.angle;
    }

    public int getPower() {
        return this.power;
    }

    public abstract AbstractCannon clone();
	public abstract void IncreasePower();
	public abstract void DecreasePower();
}