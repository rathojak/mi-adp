package cz.cvut.fit.miadp.mvcgame.model.gameObjects;

import cz.cvut.fit.miadp.mvcgame.prototype.Prototype;

/**
 * AbstractCollision
 */
public abstract class AbstractCollision extends LifetimeLimitedGO implements Prototype<AbstractCollision> {
    public AbstractCollision() {
        super();
    }
    public abstract AbstractCollision clone();
}