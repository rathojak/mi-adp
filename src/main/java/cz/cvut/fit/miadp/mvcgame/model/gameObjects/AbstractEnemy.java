package cz.cvut.fit.miadp.mvcgame.model.gameObjects;

import cz.cvut.fit.miadp.mvcgame.prototype.Prototype;
import cz.cvut.fit.miadp.mvcgame.strategy.EnemyMovingStrategy;

public abstract class AbstractEnemy extends GameObjectBase implements Prototype<AbstractEnemy>, Movable {
    protected EnemyMovingStrategy moveStr;
    public abstract AbstractEnemy clone();
}