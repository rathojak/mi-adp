package cz.cvut.fit.miadp.mvcgame.model.gameObjects;

import cz.cvut.fit.miadp.mvcgame.prototype.Prototype;

public abstract class AbstractGameInfo extends GameObjectBase implements Prototype<AbstractGameInfo>{
    public abstract void setText(String text);
    public abstract String getText();
    public abstract AbstractGameInfo clone();
}