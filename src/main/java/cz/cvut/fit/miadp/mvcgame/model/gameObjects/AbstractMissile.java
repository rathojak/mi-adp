package cz.cvut.fit.miadp.mvcgame.model.gameObjects;

import cz.cvut.fit.miadp.mvcgame.prototype.Prototype;
import cz.cvut.fit.miadp.mvcgame.strategy.MissileMovingStrategy;

public abstract class AbstractMissile extends LifetimeLimitedGO implements Prototype<AbstractMissile>, Movable {
    protected int angle;
    protected int velocity;

    public AbstractMissile() {
        super();
    }

    protected MissileMovingStrategy moveStrat;

    public void setAngle(int angle) {
        this.angle = angle;
    }
    public int getAngle() {
        return this.angle;
    }

    public void setVelocity(int vel) {
        this.velocity = vel;
    }
    public int getVelocity() {
        return this.velocity;
    }

    public abstract void move();
    public abstract AbstractMissile clone();
}