package cz.cvut.fit.miadp.mvcgame.model.gameObjects;

import java.util.Date;

/**
 * LifetimeLimitedGO
 */
public abstract class LifetimeLimitedGO extends GameObjectBase {
    protected long bornAt;
    public LifetimeLimitedGO() {
        this.bornAt = System.currentTimeMillis();
    }
    public int getAge() {
        // vrátit stáří ve vteřinách
        return (int)((System.currentTimeMillis() - this.bornAt));
    }

    protected void setBornAge(long bornAt) {
        this.bornAt = bornAt;
    }
}