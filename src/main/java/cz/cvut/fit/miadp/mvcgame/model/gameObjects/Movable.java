 package cz.cvut.fit.miadp.mvcgame.model.gameObjects;

 /**
  * Movable
  */
 public interface Movable {
    public void move();
 }