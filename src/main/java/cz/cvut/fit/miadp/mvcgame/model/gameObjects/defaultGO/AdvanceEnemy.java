package cz.cvut.fit.miadp.mvcgame.model.gameObjects.defaultGO;

import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractEnemy;
import cz.cvut.fit.miadp.mvcgame.strategy.EnemyPointToPointMoveStrategy;
import cz.cvut.fit.miadp.mvcgame.visitor.Visitor;

public class AdvanceEnemy extends AbstractEnemy {
    public AdvanceEnemy() {
        this.moveStr = new EnemyPointToPointMoveStrategy();
    }

    @Override
    public void acceptVisitor(Visitor visitor) {
        visitor.visitAdvanceEnemy(this);
    }

    @Override
    public AbstractEnemy clone() {
        Enemy clone = new Enemy();
        clone.setPosition(this.position.clone());
        return clone;
    }

    @Override
    public void move() {
        moveStr.updatePosition(this);
    }
}