package cz.cvut.fit.miadp.mvcgame.model.gameObjects.defaultGO;

import java.util.List;

import cz.cvut.fit.miadp.mvcgame.abstractFactory.AbstractFactory;
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractCannon;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractMissile;
import cz.cvut.fit.miadp.mvcgame.state.cannonShootState.SimpleShootingMode;
import cz.cvut.fit.miadp.mvcgame.visitor.Visitor;

public class Cannon extends AbstractCannon {
    public Cannon(AbstractFactory fac) {
        super(fac);
        this.shootingMode = new SimpleShootingMode();
        this.power = 1;
        this.angle = 0;
        this.setPosition(
                new Position(MvcGameConfig.CANNON_POS_X, MvcGameConfig.CANNON_POS_y));
    }
    @Override
    public AbstractMissile getMissile() {
        return new Missile(this.fac.getStrategy());
    }

    @Override
    public void moveUp() {
        if (this.position.getY() > 0)
            this.Move(0, -MvcGameConfig.CANNON_MOVE_STEP);
    }

    @Override
    public void moveDown() {
        if (this.position.getY() < MvcGameConfig.MAX_Y - 45)
            this.Move(0, MvcGameConfig.CANNON_MOVE_STEP);
    }

    @Override
    public void aimUp() {
        if (this.angle < 90) {
            this.angle += MvcGameConfig.ANGLE_STEP;
        }
    }

    @Override
    public void aimDown() {
        if (this.angle > 0) {
            this.angle -= MvcGameConfig.ANGLE_STEP;
        }
    }

    @Override
    public List<AbstractMissile> shoot() {
        return this.shootingMode.shoot(this);
    }

    @Override
    public void acceptVisitor(Visitor visitor) {
       visitor.visitCannon(this);
    }

    @Override
    public AbstractCannon clone() {
        Cannon clone = new Cannon(this.fac);
        clone.power = this.power;
        clone.setPosition(this.position.clone());
        clone.angle = this.angle;
        return clone;
    }

    @Override
    public void IncreasePower() {
        if (this.power < 5) {
            this.power += 1;
        }
    }

    @Override
    public void DecreasePower() {
        if (this.power > 0) {
            this.power -= 1;
        }
    }
}