package cz.cvut.fit.miadp.mvcgame.model.gameObjects.defaultGO;

import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractCollision;
import cz.cvut.fit.miadp.mvcgame.visitor.Visitor;

/**
 * Collision
 */
public class Collision extends AbstractCollision {
    public Collision() {
        super();
    }
    @Override
    public void acceptVisitor(Visitor visitor) {
        visitor.visitCollision(this);
    }

    @Override
    public AbstractCollision clone() {
        Collision clone = new Collision();
        clone.setPosition(this.getPosition().clone());
        clone.setBornAge(this.bornAt);
        return clone;
    }
}