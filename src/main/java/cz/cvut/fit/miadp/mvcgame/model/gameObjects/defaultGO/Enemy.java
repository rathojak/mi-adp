package cz.cvut.fit.miadp.mvcgame.model.gameObjects.defaultGO;

import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractEnemy;
import cz.cvut.fit.miadp.mvcgame.strategy.RandomEnemyMoveStrategy;
import cz.cvut.fit.miadp.mvcgame.visitor.Visitor;

public class Enemy extends AbstractEnemy {
    public Enemy() {
        this.moveStr = new RandomEnemyMoveStrategy();
    }

    @Override
    public void acceptVisitor(Visitor visitor) {
        visitor.visitEnemy(this);
    }

    @Override
    public AbstractEnemy clone() {
        Enemy clone = new Enemy();
        clone.setPosition(this.position.clone());
        clone.moveStr = this.moveStr;
        return clone;
    }

    @Override
    public void move() {
        moveStr.updatePosition(this);
    }
}