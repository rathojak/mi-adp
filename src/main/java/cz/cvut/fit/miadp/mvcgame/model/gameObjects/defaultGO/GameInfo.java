package cz.cvut.fit.miadp.mvcgame.model.gameObjects.defaultGO;

import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractGameInfo;
import cz.cvut.fit.miadp.mvcgame.visitor.Visitor;

/**
 * GameInfo
 */
public class GameInfo extends AbstractGameInfo {
    private String text;

    public GameInfo() {
        this.text = "";
    }

    @Override
    public String getText() {
        return text;
    }

    @Override
    public AbstractGameInfo clone() {
        GameInfo clone = new GameInfo();
        clone.setText(this.text);
        return clone;
    }

    @Override
    public void acceptVisitor(Visitor visitor) {
        visitor.visitGameInfo(this);
    }

    @Override
    public void setText(String text) {
        this.text = text;
    }
}