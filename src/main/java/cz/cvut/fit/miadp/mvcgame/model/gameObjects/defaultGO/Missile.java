package cz.cvut.fit.miadp.mvcgame.model.gameObjects.defaultGO;

import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractMissile;
import cz.cvut.fit.miadp.mvcgame.strategy.MissileMovingStrategy;
import cz.cvut.fit.miadp.mvcgame.visitor.Visitor;

/**
 * Missile
 */
public class Missile extends AbstractMissile {

    public Missile(MissileMovingStrategy strategy) {
        super();
        this.moveStrat = strategy;
    }

    @Override
    public void acceptVisitor(Visitor visitor) {
        visitor.visitMissile(this);
    }

    @Override
    public void move() {
        this.moveStrat.updatePosition(this);
    }

    @Override
    public AbstractMissile clone() {
        Missile clone = new Missile(this.moveStrat);
        clone.setPosition(this.position.clone());
        clone.setBornAge(this.bornAt);
        clone.setAngle(this.getAngle());
        clone.setVelocity(this.getVelocity());
        clone.moveStrat = this.moveStrat;
        return clone;
    }
}