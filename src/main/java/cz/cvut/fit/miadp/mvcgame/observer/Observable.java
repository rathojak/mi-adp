package cz.cvut.fit.miadp.mvcgame.observer;

/**
 * Observable
 */
public interface Observable {
    void attach(Observer observer);
    void detach(Observer observer);
    void notifyObservers();
}