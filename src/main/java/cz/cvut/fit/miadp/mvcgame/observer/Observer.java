package cz.cvut.fit.miadp.mvcgame.observer;

/**
 * Observable
 */
public interface Observer {
    void update();
}