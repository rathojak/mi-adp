package cz.cvut.fit.miadp.mvcgame.prototype;

/**
 * Prototype
 */
public interface Prototype<type> {
    type clone();
}