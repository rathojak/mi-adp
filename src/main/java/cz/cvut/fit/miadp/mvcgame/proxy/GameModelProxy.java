package cz.cvut.fit.miadp.mvcgame.proxy;

import java.util.List;

import cz.cvut.fit.miadp.mvcgame.command.Invoker;
import cz.cvut.fit.miadp.mvcgame.model.Model;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractMissile;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.GameObjectBase;
import cz.cvut.fit.miadp.mvcgame.observer.Observer;

/**
 * GameModelProxy
 */
public class GameModelProxy implements Model {
    private Model model;

    public GameModelProxy(Model model) {
        this.model = model;
    }

    public List<GameObjectBase> getGameObjects() {
        return this.model.getGameObjects();
    }

    public void moveUp() {
        this.model.moveUp();
    }

    public void moveDown() {
        this.model.moveDown();
    }

    @Override
    public void generateEnemies() {
        this.model.generateEnemies();
    }

    @Override
    public void setInvoker(Invoker invoker) {
        this.model.setInvoker(invoker);
    }

    @Override
    public void attach(Observer observer) {
        this.model.attach(observer);
    }

    @Override
    public void detach(Observer observer) {
        this.model.detach(observer);
    }

    @Override
    public void notifyObservers() {
        this.model.notifyObservers();
    }

    @Override
    public void setMemento(Object memento) {
        this.model.setMemento(memento);
    }

    @Override
    public Object createMement() {
        return this.model.createMement();
    }

    @Override
    public void shoot() {
        this.model.shoot();
    }

    @Override
    public void aimUp() {
        this.model.aimUp();
    }

    @Override
    public void aimDown() {
        this.model.aimDown();
    }

    @Override
    public void addMissile(AbstractMissile msl) {
        this.model.addMissile(msl);
    }

    @Override
    public void toggleShootMode() {
        this.model.toggleShootMode();
    }

    @Override
    public void setRealisticShooting() {
        this.model.setRealisticShooting();
    }

    @Override
    public void setSimpleShooting() {
        this.model.setSimpleShooting();
    }

    @Override
    public void PowerUp() {
        this.model.PowerUp();
    }

    @Override
    public void PowerDown() {
        this.model.PowerDown();
    }
}