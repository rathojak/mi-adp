package cz.cvut.fit.miadp.mvcgame.state.cannonShootState;

import java.util.ArrayList;
import java.util.List;

import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractCannon;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractMissile;

public class DoubleShootingMode implements ShootingMode {
    @Override
    public String getName() {
        return "Double shooting";
    }

    @Override
    public List<AbstractMissile> shoot(AbstractCannon cannon) {
        List<AbstractMissile> missiles = new ArrayList<>();
        AbstractMissile mis1 = cannon.getMissile();
        AbstractMissile mis2 = cannon.getMissile();
        missiles.add(mis1);
        missiles.add(mis2);

        Position cannonPos = cannon.getPosition();
        mis1.setPosition(new Position(cannonPos.getX(), cannonPos.getY() - 15));
        mis1.setAngle(cannon.getAngle());
        mis1.setVelocity(cannon.getPower());

        mis2.setPosition(new Position(cannonPos.getX(), cannonPos.getY() + 15));
        mis2.setAngle(cannon.getAngle());
        mis2.setVelocity(cannon.getPower());

        return missiles;
    }

    @Override
    public void nextMode(AbstractCannon cannon) {
        cannon.setShootingMode(new SimpleShootingMode());
    }
}