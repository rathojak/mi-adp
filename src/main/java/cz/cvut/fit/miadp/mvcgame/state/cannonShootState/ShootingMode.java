package cz.cvut.fit.miadp.mvcgame.state.cannonShootState;

import java.util.List;

import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractCannon;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractMissile;

/**
 * ShootingMode
 */
public interface ShootingMode {
    public String getName();
    public List<AbstractMissile> shoot(AbstractCannon cannon);
    public void nextMode(AbstractCannon cannon);
}