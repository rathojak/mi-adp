package cz.cvut.fit.miadp.mvcgame.state.cannonShootState;

import java.util.ArrayList;
import java.util.List;

import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractCannon;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractMissile;

/**
 * SimpleShootingMode
 */
public class SimpleShootingMode implements ShootingMode {

    @Override
    public String getName() {
        return "Simple mode";
    }

    @Override
    public List<AbstractMissile> shoot(AbstractCannon cannon) {
        List<AbstractMissile> missiles = new ArrayList<>();
        AbstractMissile mis = cannon.getMissile();
        missiles.add(mis);

        mis.setPosition(cannon.getPosition().clone());
        mis.setAngle(cannon.getAngle());
        mis.setVelocity(cannon.getPower());

        return missiles;
    }

    @Override
    public void nextMode(AbstractCannon cannon) {
        cannon.setShootingMode(new DoubleShootingMode());
    }
}
