package cz.cvut.fit.miadp.mvcgame.strategy;

import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractEnemy;

/**
 * MovingStrategy
 */
public interface EnemyMovingStrategy {
    void updatePosition(AbstractEnemy en);
    String getName();
}