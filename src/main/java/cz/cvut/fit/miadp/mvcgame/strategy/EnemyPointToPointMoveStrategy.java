package cz.cvut.fit.miadp.mvcgame.strategy;

import java.util.Random;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractEnemy;

/**
 * SimpleMoveStrategy
 */
public class EnemyPointToPointMoveStrategy implements EnemyMovingStrategy {
    private Position pos;

    public EnemyPointToPointMoveStrategy() {
        this.pos = this.generatePoint();
    }
    @Override
    public void updatePosition(AbstractEnemy en) {
        Position posE = en.getPosition();

        int xDif = pos.getX() -  posE.getX();
        int yDif = pos.getY() -  posE.getY();

        if (xDif < 0 && Math.abs(xDif) > 10) {
            en.Move(-MvcGameConfig.ENEMY_STEP_SIZE, 0);
        } else if (xDif > 0 && Math.abs(xDif) > 10) {
            en.Move(MvcGameConfig.ENEMY_STEP_SIZE, 0);
        } else if (yDif < 0 && Math.abs(yDif) > 10) {
            en.Move(0, -MvcGameConfig.ENEMY_STEP_SIZE);
        } else if (yDif > 0 && Math.abs(yDif) > 10) {
            en.Move(0, MvcGameConfig.ENEMY_STEP_SIZE);
        } else {
            this.pos = this.generatePoint();
        }
    }

    private Position generatePoint() {
        Random rnd = new Random();
        int x = -1;
        while (x < 30 || x > MvcGameConfig.MAX_X - 10) {
            x = rnd.nextInt(MvcGameConfig.MAX_X - 10);
        }
        int y = -1;
        while (y < 20 || y > MvcGameConfig.MAX_X - 10) {
            y = rnd.nextInt(MvcGameConfig.MAX_Y - 10);
        }

        return new Position(x, y);
    }

    @Override
    public String getName() {
        return "Random";
    }
}