package cz.cvut.fit.miadp.mvcgame.strategy;

import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractMissile;

/**
 * MovingStrategy
 */
public interface MissileMovingStrategy {
    void updatePosition(AbstractMissile mis);
    String getName();
}