package cz.cvut.fit.miadp.mvcgame.strategy;

import java.util.Random;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractEnemy;

/**
 * SimpleMoveStrategy
 */
public class RandomEnemyMoveStrategy implements EnemyMovingStrategy {

    @Override
    public void updatePosition(AbstractEnemy en) {
        Random rnd = new Random();
        Position pos = en.getPosition();
        int number = rnd.nextInt(4);
        switch(number) {
            case 0:
                if (pos.getX() + MvcGameConfig.ENEMY_STEP_SIZE < MvcGameConfig.MAX_X - 10) {
                    en.Move(MvcGameConfig.ENEMY_STEP_SIZE, 0);
                }
                break;
            case 1:
                if (pos.getX() - MvcGameConfig.ENEMY_STEP_SIZE > 30) {
                    en.Move(-MvcGameConfig.ENEMY_STEP_SIZE, 0);
                }
                break;
            case 2:
                if (pos.getY() + MvcGameConfig.ENEMY_STEP_SIZE < MvcGameConfig.MAX_Y - 10) {
                    en.Move(0, MvcGameConfig.ENEMY_STEP_SIZE);
                }
                break;
            case 3:
                if (pos.getY() - MvcGameConfig.ENEMY_STEP_SIZE > 20) {
                    en.Move(0, -MvcGameConfig.ENEMY_STEP_SIZE);
                }
                break;
        }
    }

    @Override
    public String getName() {
        return "Random";
    }
}