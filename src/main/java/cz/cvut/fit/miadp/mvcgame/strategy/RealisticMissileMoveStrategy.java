package cz.cvut.fit.miadp.mvcgame.strategy;

import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractMissile;

/**
 * RealisticMoveStrategy
 */
public class RealisticMissileMoveStrategy implements MissileMovingStrategy {

    // https://cs.wikipedia.org/wiki/Vrh_%C5%A1ikm%C3%BD
    // Litá to rychle a netuším proč
    @Override
    public void updatePosition(AbstractMissile missile) {
        int x0 = missile.getPosition().getX();
        int y0 = missile.getPosition().getY();
        int alpha = missile.getAngle();
        //float startingVel = missile.getVelocity();
        float startingVel = missile.getVelocity();
        float time = missile.getAge() / 100;
        // // tady musí být plus oproti vzorci, protože naše kledání znamná zvětošovat Y
        double alphaInRad = (- alpha) * Math.PI / 180;

        int x = (int) (x0 + (startingVel * time * Math.cos(alphaInRad)));
        int y = (int) (y0 + ((startingVel * time * Math.sin(alphaInRad)) + (1 / 2f * 8.91 * Math.pow(time, 2)) / 100F));

        Position newPos = new Position(x, y);
        missile.setPosition(newPos);
    }

    @Override
    public String getName() {
        return "Realistic";
    }
} 