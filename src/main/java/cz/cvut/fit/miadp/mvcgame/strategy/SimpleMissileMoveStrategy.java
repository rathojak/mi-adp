package cz.cvut.fit.miadp.mvcgame.strategy;

import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractMissile;

/**
 * SimpleMoveStrategy
 */
public class SimpleMissileMoveStrategy implements MissileMovingStrategy {

    @Override
    public void updatePosition(AbstractMissile mis) {
        Position pos = mis.getPosition();
        if (mis.getAngle() == 0) {
            pos.setX(pos.getX() + 1 * mis.getVelocity());
        } else if (mis.getAngle() == 90) {
            pos.setY(pos.getY() - 1 * mis.getVelocity());
        } else {
            pos.setX(pos.getX() + 1 * mis.getVelocity());
            pos.setY(pos.getY() - 1 * mis.getVelocity());
        }
    }

    @Override
    public String getName() {
        return "Simple";
    }
}