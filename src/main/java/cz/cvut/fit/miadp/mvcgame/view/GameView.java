package cz.cvut.fit.miadp.mvcgame.view;

import cz.cvut.fit.miadp.mvcgame.bridge.IGameGraphics;
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.controller.InputController;
import cz.cvut.fit.miadp.mvcgame.model.Model;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.GameObjectBase;
import cz.cvut.fit.miadp.mvcgame.observer.Observer;
import cz.cvut.fit.miadp.mvcgame.proxy.GameModelProxy;
import cz.cvut.fit.miadp.mvcgame.visitor.RenderingVisitor;
import cz.cvut.fit.miadp.mvcgame.visitor.Visitor;

/**
 * GameView
 */
public class GameView implements Observer {

    private Visitor rdrVisitor;
    private Model model;
    private IGameGraphics gg;

    private Boolean shouldRedraw;

    public GameView(Model model) {
        this.model = model;
        this.rdrVisitor = RenderingVisitor.getInstance();

        this.shouldRedraw = true;
        this.model.attach(this);
    }

    public InputController createController() {
        return new InputController(new GameModelProxy(model));
    }

    public void update() {
        this.shouldRedraw = true;
    }

    public void setGraphics(IGameGraphics gg) {
        this.gg = gg;
        this.rdrVisitor.setGraphics(gg);
    }

    public void render() {
        if (this.shouldRedraw) {
            this.shouldRedraw = false;
            this.gg.clear(new Position(0, 0), new Position(MvcGameConfig.MAX_X, MvcGameConfig.MAX_Y));
            for (GameObjectBase obj : this.model.getGameObjects()) {
                obj.acceptVisitor(this.rdrVisitor);
            }
        }
    }
}