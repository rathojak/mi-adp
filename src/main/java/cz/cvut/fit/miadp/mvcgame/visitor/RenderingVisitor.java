package cz.cvut.fit.miadp.mvcgame.visitor;

import cz.cvut.fit.miadp.mvcgame.bridge.IGameGraphics;
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractCannon;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractCollision;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractEnemy;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractGameInfo;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractMissile;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.defaultGO.AdvanceEnemy;

/**
 * RenderingVisitor
 */
public class RenderingVisitor implements Visitor {
    private IGameGraphics gg;
    private static RenderingVisitor single_instance = null;

    private RenderingVisitor() {
    }

    public static RenderingVisitor getInstance() {
        if (single_instance == null)
            single_instance = new RenderingVisitor();

        return single_instance;
    }

    @Override
    public void visitCannon(AbstractCannon cannon) {
        Position logoPos = cannon.getPosition();
        gg.drawImage("images/cannon.png", new Position(logoPos.getX(), logoPos.getY()));
    }

    @Override
    public void visitEnemy(AbstractEnemy enemy) {
        Position logoPos = enemy.getPosition();
        gg.drawImage("images/enemy1.png", new Position(logoPos.getX(), logoPos.getY()));
    }

    @Override
    public void visitMissile(AbstractMissile missile) {
        Position logoPos = missile.getPosition();
        gg.drawImage("images/missile.png", new Position(logoPos.getX(), logoPos.getY()));
    }

    @Override
    public void visitCollision(AbstractCollision colision) {
        Position logoPos = colision.getPosition();
        gg.drawImage("images/collision.png", new Position(logoPos.getX(), logoPos.getY()));
    }

    @Override
    public void setGraphics(IGameGraphics gg) {
        this.gg = gg;
    }

    @Override
    public void visitGameInfo(AbstractGameInfo gameInfo) {
        gg.drawText(gameInfo.getText(), new Position(MvcGameConfig.INFO_POS_X, MvcGameConfig.INFO_POS_Y));
    }

    @Override
    public void visitAdvanceEnemy(AdvanceEnemy advanceEnemy) {
        Position logoPos = advanceEnemy.getPosition();
        gg.drawImage("images/enemy2.png", new Position(logoPos.getX(), logoPos.getY()));
    }
}