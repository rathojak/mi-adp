package cz.cvut.fit.miadp.mvcgame.visitor;

import cz.cvut.fit.miadp.mvcgame.bridge.IGameGraphics;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractCannon;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractCollision;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractEnemy;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractGameInfo;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractMissile;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.defaultGO.AdvanceEnemy;

public interface Visitor {
    void visitCannon(AbstractCannon cannon);
    void visitEnemy(AbstractEnemy enemy);
    void visitMissile(AbstractMissile missile);
    void visitCollision(AbstractCollision colision);
    void visitGameInfo(AbstractGameInfo gameInfo);

    void setGraphics(IGameGraphics gg);
	void visitAdvanceEnemy(AdvanceEnemy advanceEnemy);
}