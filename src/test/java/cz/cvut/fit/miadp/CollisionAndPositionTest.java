package cz.cvut.fit.miadp;

import cz.cvut.fit.miadp.mvcgame.MvcGame;
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;

import org.junit.Assert;
import org.junit.Test;

public class CollisionAndPositionTest {
    @Test
    public void testColissionDetection() {
        Position pos1 = new Position(15, 15);
        Position pos2 = new Position(50, 50);

        Assert.assertEquals(pos1.isInRadius(pos2), false);

        pos1.setX(20);
        pos2.setY(30);

        Assert.assertEquals(pos1.isInRadius(pos2), true);
    }

    @Test
    public void testBoardLocation() {
        Position leftUp = new Position(0, 0);
        Position rightBottom = new Position(MvcGameConfig.MAX_X, MvcGameConfig.MAX_Y);

        Position myPos = new Position(50, 50);

        Assert.assertEquals(myPos.isOut(leftUp, rightBottom), false);

        myPos.setX(-1);
        myPos.setY(30);

        Assert.assertEquals(myPos.isOut(leftUp, rightBottom), true);

        myPos.setX(10);
        myPos.setY(MvcGameConfig.MAX_Y + 5);

        Assert.assertEquals(myPos.isOut(leftUp, rightBottom), true);

        myPos.setX(MvcGameConfig.MAX_X + 10);
        myPos.setY(30);

        Assert.assertEquals(myPos.isOut(leftUp, rightBottom), true);

        myPos.setX(15);
        myPos.setY(-30);

        Assert.assertEquals(myPos.isOut(leftUp, rightBottom), true);
    }
}