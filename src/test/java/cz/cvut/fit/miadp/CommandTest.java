package cz.cvut.fit.miadp;

import java.util.List;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import org.junit.Assert;
import org.junit.Test;

import cz.cvut.fit.miadp.mvcgame.model.Model;
import cz.cvut.fit.miadp.mvcgame.command.ShootCommand;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.GameObjectBase;
import cz.cvut.fit.miadp.mvcgame.command.Invoker;
import cz.cvut.fit.miadp.mvcgame.command.Memento;
import cz.cvut.fit.miadp.mvcgame.model.GameModel;;

/**
 * FactoryTest
 */
public class CommandTest {

    @Test
    public void shootCommand() {
        Model model = new GameModel();
        Invoker inv = new Invoker();
        model.setInvoker(inv);

        List<GameObjectBase> objects = (model.getGameObjects());
        Assert.assertEquals(objects.size(), 2);

        inv.addCommand(new ShootCommand(model));
        inv.invokeNextCommand();

        objects = (model.getGameObjects());
        Assert.assertEquals(objects.size(), 3);
    }

    @Test
    public void memento() {
        Model model = new GameModel();
        Invoker inv = new Invoker();
        model.setInvoker(inv);

        Memento mem1 = (Memento)model.createMement();

        inv.addCommand(new ShootCommand(model));
        inv.invokeNextCommand();

        Memento mem2 = (Memento)model.createMement();

        Assert.assertThat(mem1.getMissiles().size(), not(equalTo(mem2.getMissiles().size())));

        inv.undoCommand();

        Memento mem3 = (Memento)model.createMement();

        Assert.assertEquals(mem1.getMissiles().size(), mem3.getMissiles().size());
    }
}