package cz.cvut.fit.miadp;

import org.junit.Assert;
import org.junit.Test;

import cz.cvut.fit.miadp.mvcgame.abstractFactory.AbstractFactory;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractCannon;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbstractMissile;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.defaultGO.Cannon;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.defaultGO.Missile;
import cz.cvut.fit.miadp.mvcgame.strategy.SimpleMissileMoveStrategy;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;
/**
 * FactoryTest
 */
public class FactoryTest {

    @Test
    public void testFactory() {
        AbstractFactory facMock = mock(AbstractFactory.class);
        when(facMock.createCannon()).thenReturn(new Cannon(facMock));
        when(facMock.createMissile()).thenReturn(new Missile(new SimpleMissileMoveStrategy()));

        AbstractCannon cann = facMock.createCannon();
        List<AbstractMissile> missiles = cann.shoot();
        Assert.assertEquals(missiles.size(), 1);
    }
}